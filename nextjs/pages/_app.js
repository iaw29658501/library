import '../styles/globals.css'
import 'tailwindcss/tailwind.css'
export async function getStaticProps(context) {
  const res = await fetch(`https://.../data`)
  const data = await res.json()

  if (!data) {
    return {
      notFound: true,
    }
  }

  return {
    props: { data }, // will be passed to the page component as props
  }
}
function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />
}

export default MyApp
