import Head from 'next/head'
import {useState} from "react";
import Link from 'next/link'

export default function Home({inputs}) {
    const [label, setLabel] = useState('');
    function handleSubmit(event) {

    }
  return (
    <div className="py-2 flex flex-col justify-start items-center h-screen">
      <Head>
        <title>Home</title>
        <meta name="description" content="" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
        <div className="h-30 bg-blue-700 w-full flex justify-center space-x-4 text-white ">
            <Link href="campaigns/new">New</Link>
            <Link href="campaigns/">List</Link>
        </div>
      <div className="shadow-xl p-10 bg-white max-w-xl rounded">
          <div className="">
            <h3>Welcome to the home page</h3>
          </div>
      </div>
    </div>
  )
}