import Link from "next/link";

export default function Index({campaigns}) {
    return (<div className="p-4 bg-blue-200">
        {campaigns.map(c=> (
            <div className="p-4 m-2 bg-white shadow">
                <Link href={c["@id"]}>{c.name}</Link>
            </div>
        ))}
    </div>);
}
export const getStaticProps = async() => {
    const res = await fetch('http://symfony.localhost/campaigns');
    const data = await res.json();
    return {
        props: {
            "campaigns":data['hydra:member']
        }
    }
}