

import Head from 'next/head'
import {useState} from "react";
import Link from 'next/link'

export default function Home({inputs}) {
    const [label, setLabel] = useState('');
    function handleSubmit(event) {

    }
    return (
        <div className="min-h-screen py-2 flex flex-col justify-center items-center h-screen">
            <Head>
                <title>Create Next App</title>
                <meta name="description" content="Generated by create next app" />
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <div className="shadow-xl p-10 bg-white max-w-xl rounded">

                <div className="">
                    <h3 class="text-center text-xl">Login</h3>
                </div>
                <form onSubmit={handleSubmit}>
                    <div className="">
                        <div className="mb-3 pt-0">
                            <label className="px-3 py-3 text-blueGray-400">Email:</label>
                            <input type="text" placeholder="username"
                                   className="px-3 py-3 placeholder-blueGray-300 text-blueGray-600 relative bg-white bg-white rounded text-sm border-0 shadow outline-none focus:outline-none focus:ring w-full"/>
                        </div>
                        <div className="mb-3 pt-0">
                            <label className="px-3 py-3 text-blueGray-400">Password:</label>
                            <input type="password" placeholder="password"
                                   className="px-3 py-3 placeholder-blueGray-300 text-blueGray-600 relative bg-white bg-white rounded text-sm border-0 shadow outline-none focus:outline-none focus:ring w-full"/>
                        </div>
                        <button type="submit" class="bg-blue-400 hover:bg-blue-500 text-white px-4 py-2 rounded">Submit</button>
                    </div>
                </form>

            </div>
        </div>
    )
}
// export const getStaticProps = async() => {
//     const res = await fetch('http://symfony.localhost/campaigns')
//     let inputs = await res.json();
//     inputs = inputs['hydra:member']
//     return {
//         props: {
//             inputs: inputs
//         }
//     }
// }
